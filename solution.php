

//user controller

class UsersController extends \cmts\CMTSModuleController {

    public function getUsers() {

       $users =  $this->model->getMGFItems('users', $filter = null);

$this->set(“users”,$users);
    }

}

//user model

use \MGF;

class UsersModel extends \cmts\CMTSModuleModel {

    public function getItems($user, $format, $filter) {
        $users = MGF::get_MGF($user, $format, $filter);
        return $users;
    }

}




//user view file

<table>
    <thead><tr><th>name</th><th>type</th><th>description</th></tr></thead>
    <tbody>
        <?php foreach ($users as $user) { ?>
            <tr>
                <td><?php echo $user['name']; ?></td>
                <td><?php echo $user['type']; ?></td>
                <td><?php echo $user['description']; ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>







//external file for fetching and caching data
<?php
define('CACHE_JSON_ROOT', 'location of json folder');
define('MGF_MAXAGE', 'time to live');
define('APIKEY', 'static apikey');
define('MGF_URL', 'http://www.mgf.ltd.uk/software-test/api.php');


class MGF {

    public static function get_MGF($module, $format, $filter = null) {
        if (!$format || !$module) {
            return array();
        }
        $data = self::load($module, $format, $filter);
        if (!$data) {
            $data = self::fetch($module, $lang, $uid, $offergroup);
            if (!is_array($data)) {
                return array();
            }
        }
    }


    public static function load($module, $format, $filter) {
        $filename = self::key($module, $filter);
        if (($data = Mem::get($filename))) {
            return $data;
        }
        $cachefile = CACHE_JSON_ROOT . $filename . '.json';
        if (file_exists($cachefile)) {
            $data = json_decode(file_get_contents($cachefile), true);
            if (is_array($data)) {
                Mem::set($filename, $data, MGF_MAXAGE);
                return $data;
            }
        }
        return false;
    }

    public static function fetch($module, $format, $filter) {
        $info['url'] = MGF_URL;
        $info['file'] = self::key($module, $filter);
        $info['user_data'] = array($filter, 'apikey' => APIKEY);
        $data = self::loadUrl($info['url'], $format, $info['user_data'], $info['file']);
        if ($data === FALSE) {
            return array();
        }

        Mem::set(self::key($module, $filter), $data);
        return $data;
    }

    public static function loadUrl($url, $format, $info_data, $filename = null) {
        $json = null;
        if (!$filename) {
            $filename = 'ext-' . substr(hash('md4', $url), 0, 10);
            if (($data = Mem::get($filename))) {
                return $data;
            }
            $json = @file_get_contents(CACHE_JSON_ROOT . $filename . '.' . $format);
        }
        if (!$json) {
            $options = array(
                'http' => array(
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method' => 'POST',
                    'content' => http_build_query($info_data)
                )
            );
            $data_context = stream_context_create($options);
            $data = file_get_contents($url, false, $data_context);
        }
        if ($data) {
            if ($format == 'json') {
                $json = json_decode($data, true);
                if (!is_array($json)) {
                    $json = array();
                }
            }
            Mem::set($filename, $data, FMTS_MAXAGE);
            $cachefile = CACHE_JSON_ROOT . $filename . '.' . $format;
            file_put_contents($cachefile . '.new', $data);
            @chmod($cachefile . '.new', 0777);
            rename($cachefile . '.new', $cachefile);
            return $data;
        } else {
            die('Failed to read data MGF ' . $filename);
        }
    }

    private static function key($module, $filter) {
        return $module . ($filter ? '-' . $filter . '-' : '');
    }
    
    //convert the data to what ever format

public function convert($module,$from,$to){
  $url = //get url from the static string
  $data = self::loadUrl($url, $from, $info_data);
  $to_data = getOutput($data,$to);
  return $to_data;
}

}
?>



//script for clearing cache
<?php
header('Content-Type: text/plain; charset=utf-8');

function clearCache($folder) {
    if ($folder) {
        if ($folder == '*') {
            $dirs = glob(__DIR__ . '/*', GLOB_ONLYDIR);
            $dirs[] = 'mem';
        } else {
            $dirs = array(__DIR__ . '/' . $folder);
        }
        foreach ($dirs as $cachedir) {
            $counter = 0;
            if (is_dir($cachedir)) {

                foreach (glob($cachedir . '/*') as $file) {
                    if (is_file($file)) {
                        @unlink($file);
                        if (!file_exists($file)) {
                            $counter++;
                        } else {
                            echo 'Error deleting ' . $file . "!\n";
                        }
                    }
                }
                echo('Cleared ' . $counter . ' files in ' . $cachedir . ".\n");
            } else {
                echo 'Error:' . $cachedir . "\n";
            }
        }
    }
}
?>

//ajax call script for computing average salary etc

<script>
$type = 'averageAge';
    $.post('/Users/user.json', {request:$type},function(response){
        for (var name in response) {
            console.log(name['type'],name['average']);
        }
    }
    );

</script>

//ajax php for computing age, salary etc
<?php 
Class UsersController {

function UserJSON($data){
$action = $data['request'];
//switch
    $data = getAverage($type,$period);//this call group by query with operation on expression to get average of each user 
    header('content-type:application/json;charset=utf-8');
        echo json_encode($data);
        die();
    
}
}

?>